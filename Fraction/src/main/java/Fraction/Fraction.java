package Fraction;

public class Fraction
{
    private int denominateur;
    private int numerateur;

    public Fraction(final int denominateur, final int numerateur){
        this.denominateur = denominateur;
        this.numerateur = numerateur;
    }

    public Fraction( final int numerateur){
        this.denominateur = 1;
        this.numerateur = numerateur;
    }


    public Fraction(){
        this.denominateur = 1;
        this.numerateur = 0;
    }
}
